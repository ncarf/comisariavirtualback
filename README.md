# Sistemas distribuidos - PEP 1
 
**Alumnos:** Nicolás Cárcamo, Mateo Sepúlveda <br />
**Profesor:** Joaquín Villagra <br />
**Fecha:** 22 de noviembre de 2020 <br />
**Semestre:** 2020-II
 
## Introducción
 
En el siguiente documento se explican brevemente las características del sistema desarrollado para simular el proceso de solicitud de permisos de Comisaría Virtual, y posteriormente, se analiza si el sistema cumple con los atributos que debe tener un sistema distribuido. Finalmente, se incluyen instrucciones de instalación y ejecución para la plataforma.
 
## Desarrollo de sistema legado
 
Se elige como lenguaje de backend Java (JDK 11), utilizando el framework Spring Boot, complementado con la herramienta de gestión de proyectos Apache Maven. Para frontend, se utiliza el framework Vue.js, que a su vez emplea el lenguaje Javascript. Vue.js es complementado con librerías tales como Vue Router, Vuetify, Axios y Moment.js. Como motor de base de datos se usa PostgreSQL 13, que provee bases de datos relacionales.
 
La arquitectura es la siguiente:

![Arquitectura](/uploads/1272e6859ab5d0881049aec0acf2a19d/Arquitectura.png)
 
**Notas sobre la plataforma**
- Al solicitar un permiso, este empezará su vigencia 15 minutos después desde la hora en que se cargó el formulario, y terminará su vigencia 3 horas después.
- La funcionalidad de envío de correos fue probada solo con un correo de Gmail creado exclusivamente para este trabajo. La configuración respectiva está en el archivo PermisoController.java, en el método sendmailVerificacion.
 
## Análisis de sistema legado
 
A continuación se abordan algunas de las características más importantes de un sistema distribuido, y se analiza si el sistema legado las cumple.
 
**Capacidad de lidiar con heterogeneidad**
 
El sistema desarrollado no requiere lidiar con la heterogeneidad del hardware, ya que todos los componentes de su arquitectura (módulos de frontend y backend, y motor de base de datos) están diseñados y configurados para ejecutarse en un solo dispositivo, a diferencia de un sistema distribuido, cuyos servicios y datos pueden estar particionados y repartidos entre diferentes dispositivos, con características de hardware variadas.
 
**Poner recursos a disposición**
 
El sistema legado está conectado a un servicio de correo electrónico, el cual comparte información con el usuario final que está solicitando un permiso en la plataforma. Esto se hace mediante el envío de un correo electrónico al usuario, donde se especifica el identificador del permiso generado y la información que el usuario ingresó en el formulario. Por otra parte, el sistema permite a su administrador hacer uso del recurso del correo electrónico de forma remota. Por lo que, tanto del punto de vista del usuario final, como del administrador del sistema, este sí cumple con esta característica de un sistema distribuido, facilitando el acceso de información al usuario de manera eficiente.
 
**Transparencia**
 
Este concepto se refiere a ocultar ante el usuario el hecho de que un sistema distribuido está compuesto de varios dispositivos, con diferentes características técnicas y repartidos en diferentes ubicaciones. En otras palabras, el usuario debe percibir el sistema como si corriera en un único computador.

El concepto de transparencia no se aplica en este caso, ya que no es necesario ocultar que el sistema está conformado por varios computadores, pues solo está conformado por uno, estando diseñado y configurado para ejecutarse en un único equipo.
 
**Apertura**
 
Un sistema distribuido es abierto si sus funcionalidades pueden ser extendidas de diversas maneras, y se basa en entregar un mecanismo uniforme de comunicación entre procesos e interfaces, con la finalidad de acceder a recursos compartidos. Dado que nuestro sistema no está diseñado contemplando su extensión mediante otros componentes (no cumple con el principio de fácil extensión), ni el permitir que sus procesos se comuniquen generando implementaciones diferentes (interoperabilidad), no cumple con la característica de apertura.
 
**Escalabilidad**
 
Este concepto se refiere a la posibilidad de tomar un sistema y agregarle mayores recursos (escalabilidad horizontal), o mejorar la capacidad de dichos recursos (escalabilidad vertical), con el fin de adecuar dicho sistema a una mayor carga de trabajo. Hoy en día, la escalabilidad es un concepto relevante ante el crecimiento exponencial de los usuarios y los datos presentes en la web.
 
El sistema legado que se desarrolló no cumple el concepto de escalabilidad horizontal, dado que su backend no contempla conceptos tales como el particionamiento para base de datos, la replicación o el caché, ni tampoco el poder agregar más nodos de trabajo que realicen el trabajo requerido para la plataforma. Por otro lado, el sistema sí cumple con la escalabilidad vertical, puesto que al mejorar el recurso de hardware en el que se ejecuta el sistema (un computador), la plataforma será capaz de alcanzar un mayor rendimiento y una mayor capacidad de procesamiento.
 
**Capacidad de respuesta del sistema**
 
Se efectuó una serie de pruebas de carga utilizando la herramienta Artillery (https://artillery.io/), las cuales consistían en realizar cierta cantidad de peticiones al backend durante cierto intervalo de tiempo, y se obtuvieron los siguientes resultados:


Prueba # | Duración (seg) | Tasa de llegada peticiones (peticiones/seg) | Peticiones lanzadas | Peticiones completadas con éxito | Porcentaje de éxito
--- | --- | --- | --- |--- |---
1 | 30 | 10 | 300 | 300 | 100%
2 | 30 | 30 | 900 | 781 | 86,8%
3 | 30 | 50 | 1500 | 807 | 53,8%
4 | 30 | 100 | 3000 | 622 | 20,7%
 
Al incrementar la tasa de llegada de peticiones, el porcentaje de éxito (peticiones completadas con éxito/peticiones lanzadas) disminuye notablemente, lo cual implica que el sistema no es escalable, ya que las peticiones realizadas al backend dejan de ser procesadas correctamente cuando llegan con una frecuencia demasiado alta.
 
## Conclusiones
 
Del análisis anterior se infiere que el sistema legado cumple con ciertos atributos de un sistema distribuido, como el poner recursos a disposición del usuario, y la escalabilidad vertical. Sin embargo, hay otras características con las que o no cumple, o simplemente no se aplican en este caso, dado el hecho de que el sistema está diseñado y configurado para ejecutarse en un único computador.
 
## Anexo - Instrucciones de ejecución de la plataforma
 
1. Descargar o clonar los proyectos de frontend y backend desde los repositorios:
    - https://gitlab.com/ncarcamof/comisariavirtualfront/
    - https://gitlab.com/ncarcamof/comisariavirtualback/
2. Instalar en el computador las siguientes librerías de frontend (por ejemplo, mediante el sistema de gestión de paquetes npm):
    - Vue (comando de npm: "npm install vue")
    - Vue Router (comando de npm: "npm install vue-router")
    - Vuetify (comando de npm, a ejecutarse desde el directorio del proyecto 
de frontend: "vue add vuetify")
    - Axios (comando de npm: "npm install axios")
    - Moment.js (comando de npm: "npm install moment --save")
3. En el proyecto de backend, modificar el archivo application.properties, ubicado en "src/main/resources/".
    - "spring.datasource.password = (CONTRASEÑA)", donde (CONTRASEÑA) es la clave de usuario para acceder a PostgreSQL. **NOTA:** Se asume que PostgreSQL usará el puerto por defecto (5432) y la base de datos por defecto (postgres).
4. Ejecutar el proyecto de backend mediante la herramienta que desee, por ejemplo, mediante un IDE como IntelliJIdea, o mediante Maven (ejecutando "mvn spring-boot:run" desde el directorio backend del proyecto). Debe cargarse en esta URL: http://localhost:8080
5. Ejecutar el proyecto de frontend (mediante npm, se puede hacer ejecutando el comando "npm run serve" desde el directorio frontend del proyecto).
6. Dirigirse al sitio web del frontend (suele ser http://localhost:8081) y utilizar el sitio, llenando el formulario. Una vez completado adecuadamente, se mostrará un mensaje de confirmación y se enviará un correo electrónico para el mismo fin. Para verificar que los datos fueron ingresados a la base de datos de PostgreSQL, se puede utilizar la aplicación pgAdmin incluida con PostgreSQL, para revisar si la tabla "Permisos" fue creada con todas sus columnas, y si los datos ingresados mediante el sitio web han sido agregados como filas a la tabla de permisos.
